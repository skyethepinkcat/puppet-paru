class paru ($uid = 231,
            $puppet_managed_sudo = true) {
  class {"paru::install" :}
  user {'Paru Build User':
    name => "parubuild",
    ensure => "present",
    uid => $uid,
    system => true,
    home => "/var/lib/puppet-paru/",
    managehome => true,
    require => Class['paru::install']
  }
  if puppet_managed_sudo {
    sudo::conf { 'parubuild':
      content  => "parubuild ALL = (root)  NOPASSWD: /usr/bin/pacman *",
    }
  } else {
    file { '/etc/sudoers.d/10_parubuild':
      source   => 'puppet:///modules/paru/parubuild.sudo',
      owner    => 'root',
      group    => 'root',
      mode     => '0440'
    }
  }
}
