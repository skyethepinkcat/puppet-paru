# Puppet-Paru

This is essentially a reimplementation of the "pacman" package
provider, but it uses paru instead of yaourt. This is mostly for my
own personal use, but feel free to use it yourself.

## Limitations

This module will not install paru, and it uses a very ugly method to
build packages using paru even while running as root. Specifically, it
creates a system user ``parubuild``, and gives it sudo access to
pacman. This allows ``parubuild`` to act as a build user for paru. Let
me be clear, this is almost certainly a horrible and unsafe way to do
this. The way this should be done is to build the package as
``parubuild``and then install it as root, however paru offers
no way to do this easily. Do not use this for secure systems.

## Configuration

The paru class offers two configuration options. The first is ``uid``,
which allows you to change the uid of ``parubuild``. By default it is
231, a totally random number I selected, but if your systems use that
for something else, it is reccomended you change it.

The second is ``puppet_managed_sudo``. By default this option is true,
and uses the
[saz-sudo](https://forge.puppet.com/modules/saz/sudo/readme) module to
manage your sudo configuration. **This will overwrite your current
sudo file** so make sure your sudo configuration is done through
puppet. If set to false, it will simply create a file in
``/etc/sudoers.d/``, but you will have to manually make sure sudo
checks there.
